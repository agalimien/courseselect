import 'package:course_select/db_service/course/course_service.dart';
import 'package:course_select/db_service/db_model/db_course.dart';
import 'package:course_select/db_service/tutor/tutor_service.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  //AllCourseList should return with 3 courses
  test('testAllCourseListShouldReturnWith3Courses', () async {
    CourseService courseService = CourseService();
    await courseService.open('test/assets/course_select.db');
    List courseList = await courseService.getAllCourseList();
    expect(courseList.length, 3);
  });

  //InsertCourse should not return 0
  //0 means insert failed
  test('testInsertCourseShouldNotReturn0', () async {
    CourseService courseService = CourseService();
    await courseService.open('test/assets/course_select.db');
    int id = await courseService.insertCourse(DbCourse(
        10,
        'Math',
        4,
        DateTime.now().millisecondsSinceEpoch,
        DateTime.now().add(const Duration(hours: 2)).millisecondsSinceEpoch,
        'Math is fun!'));
    expect(id, isNot(0));
  });

  //AllTutorList should return with 2 tutors
  test('testAllTutorListShouldReturnWith2Tutors', () async {
    TutorService tutorService = TutorService();
    await tutorService.open('test/assets/course_select.db');
    List tutorList = await tutorService.getAllTutorList();
    expect(tutorList.length, 2);
  });
}