# 選課專案

 - widget: `lib/widgets`
 - view model: `lib/models`

## Screenshots

<p float="left">
    <img src="1.jpg" width="200">
    <img src="2.jpg" width="200">
    <img src="3.jpg" width="200">
</p>

# Design Service Class
- service class: `lib/db_service/course`, `lib/db_service/tutor`
- model class: `lib/db_service/db_model`

# Write unit test for service
- test cases: `test/service_api_test.dart`