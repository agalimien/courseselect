import 'package:flutter/material.dart';

class CourseDetailPage extends StatelessWidget {
  final String courseName;
  final String timeDescription;
  const CourseDetailPage({super.key, required this.courseName, required this.timeDescription});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(courseName),
      ),
      body: Center(
        child: Text(timeDescription),
      ),
    );
  }
}
