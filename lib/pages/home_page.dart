import 'package:course_select/widgets/tutor_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../model/app_data_model.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final tutorAndCourses = context.read<AppDataViewModel>().tutorAndCourses;

    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.arrow_back),
        title: const Text('講師清單'),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(16),
        itemBuilder: (context, index) {
          final tutorAndCourse = tutorAndCourses[index];
          return TutorCard(
            tutorModel: tutorAndCourse.tutorModel,
            courseModels: tutorAndCourse.courseModels,
          );
        },
        separatorBuilder: (ctx, idx) => const SizedBox(height: 16),
        itemCount: tutorAndCourses.length,
      ),
    );
  }
}
