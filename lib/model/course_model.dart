class CourseModel {
  final String courseName;
  final String timeDescription;

  const CourseModel({
    required this.courseName,
    required this.timeDescription,
  });
}