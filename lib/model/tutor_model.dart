class TutorModel {
  final String imgAsset;
  final String title;
  final String name;

  const TutorModel({
    required this.imgAsset,
    required this.title,
    required this.name,
  });
}