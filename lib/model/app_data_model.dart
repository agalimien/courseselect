import 'package:course_select/common/images/app_images.dart';
import 'package:course_select/model/tutor_model.dart';

import 'course_model.dart';

class TutorAndCourse {
  final TutorModel tutorModel;
  final List<CourseModel> courseModels;

  const TutorAndCourse({
    required this.tutorModel,
    required this.courseModels,
  });
}

class AppDataViewModel {
  final List<TutorAndCourse> tutorAndCourses = [
    const TutorAndCourse(
      tutorModel: TutorModel(
        imgAsset: AppImages.alex,
        title: 'Professor',
        name: 'Alex Chen',
      ),
      courseModels: [
        CourseModel(
          courseName: '基礎程式設計',
          timeDescription: '每週二, 10:00 ~ 12:00',
        ),
        CourseModel(
          courseName: '計算機網路',
          timeDescription: '每週四, 14:00 ~ 16:00',
        ),
        CourseModel(
          courseName: '訊號與系統',
          timeDescription: '每週五, 10:00 ~ 12:00',
        ),
      ],
    ),
    const TutorAndCourse(
      tutorModel: TutorModel(
        imgAsset: AppImages.jake,
        title: 'Instuctor',
        name: 'Jake Wu',
      ),
      courseModels: [
        CourseModel(
          courseName: 'Machine Learning',
          timeDescription: '每週二, 10:00 ~ 12:00',
        ),
        CourseModel(
          courseName: 'Bioinformatics',
          timeDescription: '每週四, 14:00 ~ 16:00',
        ),
        CourseModel(
          courseName: 'Design Thinking',
          timeDescription: '每週五, 10:00 ~ 12:00',
        ),
      ],
    ),
    const TutorAndCourse(
      tutorModel: TutorModel(
        imgAsset: AppImages.joseph,
        title: 'Demonstrator',
        name: 'Joseph Chang',
      ),
      courseModels: [
        CourseModel(
          courseName: 'Medical Image Processing',
          timeDescription: '每週二, 10:00 ~ 12:00',
        ),
        CourseModel(
          courseName: 'Random Process',
          timeDescription: '每週四, 14:00 ~ 16:00',
        ),
        CourseModel(
          courseName: 'Numerical Analysis',
          timeDescription: '每週五, 10:00 ~ 12:00',
        ),
      ],
    ),
    const TutorAndCourse(
      tutorModel: TutorModel(
        imgAsset: AppImages.michael,
        title: 'Assistant Professor',
        name: 'Michael Cho',
      ),
      courseModels: [
        CourseModel(
          courseName: 'Physics',
          timeDescription: '每週二, 10:00 ~ 12:00',
        ),
        CourseModel(
          courseName: 'Quantum Mechanics',
          timeDescription: '每週四, 14:00 ~ 16:00',
        ),
        CourseModel(
          courseName: 'Entropy and Information',
          timeDescription: '每週五, 10:00 ~ 12:00',
        ),
      ],
    ),
    const TutorAndCourse(
      tutorModel: TutorModel(
        imgAsset: AppImages.sigmund,
        title: 'Lecturer',
        name: 'Sigmund Kim',
      ),
      courseModels: [
        CourseModel(
          courseName: 'Encryption',
          timeDescription: '每週二, 10:00 ~ 12:00',
        ),
        CourseModel(
          courseName: 'Data Structure',
          timeDescription: '每週四, 14:00 ~ 16:00',
        ),
        CourseModel(
          courseName: 'Algorithm',
          timeDescription: '每週五, 10:00 ~ 12:00',
        ),
      ],
    ),
  ];
}
