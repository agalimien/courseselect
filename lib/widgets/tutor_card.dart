import 'package:course_select/widgets/card_divider.dart';
import 'package:course_select/widgets/tutor_item.dart';
import 'package:flutter/material.dart';

import '../model/course_model.dart';
import '../model/tutor_model.dart';
import 'course_item.dart';

class TutorCard extends StatefulWidget {
  final TutorModel tutorModel;
  final List<CourseModel> courseModels;
  const TutorCard({super.key, required this.tutorModel, required this.courseModels});

  @override
  State<TutorCard> createState() => _TutorCardState();
}

class _TutorCardState extends State<TutorCard> {
  var isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.black87),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TutorItem(
            tutorModel: widget.tutorModel,
            onTap: () {
              setState(() {
                isExpanded = !isExpanded;
              });
            },
            isExpanded: isExpanded,
          ),
          if(isExpanded) const CardDivider(),
          if(isExpanded) Column(
            mainAxisSize: MainAxisSize.min,
            children: widget.courseModels.map((courseModel) => CourseItem(courseModel: courseModel)).toList(),
          ),
        ],
      ),
    );
  }
}
