import 'package:flutter/material.dart';

class CardDivider extends StatelessWidget {
  const CardDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: 16),
        Divider(
          height: 1,
          thickness: 1,
        ),
        SizedBox(height: 16),
      ],
    );
  }
}
