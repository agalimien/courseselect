import 'package:course_select/model/tutor_model.dart';
import 'package:flutter/material.dart';

class TutorItem extends StatelessWidget {
  final TutorModel tutorModel;
  final VoidCallback onTap;
  final bool isExpanded;
  const TutorItem({super.key, required this.tutorModel, required this.onTap, this.isExpanded = false});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ClipOval(child: Image.asset(tutorModel.imgAsset, width: 65, height: 65, fit: BoxFit.cover)),
        const SizedBox(width: 8),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              tutorModel.title,
              style: TextStyle(
                fontSize: 12,
                color: Colors.black.withOpacity(0.75),
              ),
            ),
            const SizedBox(height: 4),
            Text(
              tutorModel.name,
              style: const TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
        const Spacer(),
        IconButton(onPressed: onTap, icon: Icon(isExpanded ? Icons.remove : Icons.add)),
      ],
    );
  }
}