import 'package:course_select/model/course_model.dart';
import 'package:flutter/material.dart';

import '../pages/course_detail_page.dart';

class CourseItem extends StatelessWidget {
  final CourseModel courseModel;
  const CourseItem({super.key, required this.courseModel});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.calendar_month),
      title: Text(courseModel.courseName),
      subtitle: Text(courseModel.timeDescription),
      trailing: const Icon(Icons.arrow_forward_ios),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CourseDetailPage(courseName: courseModel.courseName, timeDescription: courseModel.timeDescription))
        );
      },
    );
  }
}
