abstract class AppImages {
  static const alex = 'assets/images/alex.jpg';
  static const jake = 'assets/images/jake.jpg';
  static const joseph = 'assets/images/joseph.jpg';
  static const michael = 'assets/images/michael.jpg';
  static const sigmund = 'assets/images/sigmund.jpg';
}