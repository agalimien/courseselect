class DbCourse {
    final int id;
    final String name;
    final int weekday;
    final int startTime; //milliseconds
    final int endTime; //milliseconds
    final String description;

    const DbCourse(
        this.id,
        this.name,
        this.weekday,
        this.startTime,
        this.endTime,
        this.description,
    );

    factory DbCourse.fromMap(Map<String, dynamic> json) => DbCourse(
        json['id'] as int,
        json['name'] as String,
        json['weekday'] as int,
        json['startTime'] as int,
        json['endTime'] as int,
        json['description'] as String,
    );

    //toMap
    Map<String, dynamic> toMap() => {
        'id': id, //id is auto increment
        'name': name,
        'weekday': weekday,
        'startTime': startTime,
        'endTime': endTime,
        'description': description,
    };
}