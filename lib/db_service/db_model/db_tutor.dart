class DbTutor {
  final int id;
  final String name;
  final String title;
  final String imgLink;

  const DbTutor(
    this.id,
    this.name,
    this.title,
    this.imgLink,
  );

  factory DbTutor.fromMap(Map<String, dynamic> json) => DbTutor(
    json['id'] as int,
    json['name'] as String,
    json['title'] as String,
    json['imgLink'] as String,
  );

  //toMap
  Map<String, dynamic> toMap() => {
    'id': id, //id is auto increment
    'name': name,
    'title': title,
    'imgLink': imgLink,
  };

}