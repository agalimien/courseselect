import 'package:course_select/db_service/db_model/db_course.dart';
import 'package:sqflite/sqflite.dart';

class CourseService {
  final String tableCourse = 'course';
  final String columnId = 'id';

  late Database db;

  Future open(String path) async {
    db = await openDatabase(path, version: 1);
  }

  //課程列表 API (Read)
  Future<List<DbCourse>> getAllCourseList() async {
    List<DbCourse> courseList = (await db.rawQuery('SELECT * FROM $tableCourse')).map((e) => DbCourse.fromMap(e)).toList();
    return courseList;
  }

  //授課講師所開課程列表 API (Read)
  Future<List<DbCourse>> getTutorCourseList(int tutorId) async {
    List<DbCourse> courseList = (await db.rawQuery('SELECT * FROM $tableCourse WHERE tutor_id=$tutorId'))
        .map((e) => DbCourse.fromMap(e)).toList();
    return courseList;
  }

  //建立新課程 API (Create)
  Future<int> insertCourse(DbCourse course) async {
    int id = await db.insert(tableCourse, course.toMap());
    return id;
  }

  //更新課程內容 API (Update)
  Future<int> updateCourse(DbCourse course) async {
    int id = await db.update(tableCourse, course.toMap(), where: '$columnId=?', whereArgs: [course.id]);
    return id;
  }

  //刪除課程 API (Delete)
  Future<int> deleteCourse(int id) async {
    int result = await db.delete(tableCourse, where: '$columnId=?', whereArgs: [id]);
    return result;
  }


  Future close() async => db.close();
}