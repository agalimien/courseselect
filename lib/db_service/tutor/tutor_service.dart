import 'package:course_select/db_service/db_model/db_tutor.dart';
import 'package:sqflite/sqflite.dart';

class TutorService {
  final String tableTutor = 'tutor';
  final String columnId = 'id';

  late Database db;

  Future open(String path) async {
    db = await openDatabase(path, version: 1);
  }

  //授課講師列表 API (Read)
  Future<List<DbTutor>> getAllTutorList() async {
    List<DbTutor> tutorList = (await db.rawQuery('SELECT * FROM $tableTutor'))
        .map((e) => DbTutor.fromMap(e)).toList();
    return tutorList;
  }

  //建立新講師 API (Create)
  Future<int> insertTutor(DbTutor tutor) async {
    int id = await db.insert(tableTutor, tutor.toMap());
    return id;
  }


  Future close() async => db.close();
}